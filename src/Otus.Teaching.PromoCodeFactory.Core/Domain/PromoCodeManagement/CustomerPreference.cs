﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("CustomerPreference")]
    public class CustomerPreference : BaseEntity
    {
        public CustomerPreference()
        {
            Id = Guid.NewGuid();
        }

        public Guid CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        public Guid PreferenceId { get; set; }
        
        [ForeignKey("PreferenceId")]
        public Preference Preference { get; set; }
    }
}
