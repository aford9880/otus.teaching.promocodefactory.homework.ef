﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("Customers")]
    public class Customer
        : BaseEntity
    {
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public ICollection<CustomerPreference> CustomerPreferences { get; set; }

        public ICollection<PromoCode> Promocodes { get; set; }

        /// <summary>
        /// Удаление связанных сущностей
        /// </summary>
        /// <param name="customerPreferenceRepository"></param>
        /// <param name="promoCodeRepository"></param>
        public void DeleteRelatedEntities(IRepository<CustomerPreference> customerPreferenceRepository, IRepository<PromoCode> promoCodeRepository)
        {
            var customerPreferences = customerPreferenceRepository.GetAllAsync(x => x.CustomerId == Id).Result;
            if (customerPreferences != null && customerPreferences.Any())
            {
                customerPreferenceRepository.DeleteRangeAsync(customerPreferences).Wait();
            }
         
            var promoCodes = promoCodeRepository.GetAllAsync(x => x.CustomerId == Id).Result;
            if (promoCodes != null && promoCodes.Any())
            {
                promoCodeRepository.DeleteRangeAsync(promoCodes).Wait();
            }
        }
    }
}