﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("PromoCodes")]
    public class PromoCode
        : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Code { get; set; }

        [Required]
        [MaxLength(100)]
        public string ServiceInfo { get; set; }

        [Required]
        public DateTime BeginDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [MaxLength(100)]
        public string PartnerName { get; set; }

        public Guid CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        public Guid PartnerManagerId { get; set; }

        [ForeignKey("EmployeeId")]
        public Employee PartnerManager { get; set; }

        
        public Guid PreferenceId { get; set; }
        [ForeignKey("PreferenceId")]
        public Preference Preference { get; set; }

        [Required]
        public bool IsUsed { get; set; }
    }
}