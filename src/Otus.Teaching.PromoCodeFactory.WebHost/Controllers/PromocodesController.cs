﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private const int PromocodeDelay = 10;

        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository, IRepository<CustomerPreference> customerPreferenceRepository, IRepository<Preference> preferenceRepository)
        {
            _promocodeRepository = promocodeRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var response = new List<PromoCodeShortResponse>();

            var promocodes = await _promocodeRepository.GetAllAsync();
            foreach (var promocode in promocodes)
            {
                response.Add(new PromoCodeShortResponse
                {
                    Id = promocode.Id,
                    Code = promocode.Code,
                    ServiceInfo = promocode.ServiceInfo,
                    BeginDate = promocode.BeginDate.ToShortDateString(),
                    EndDate = promocode.EndDate.ToShortDateString(),
                    PartnerName = promocode.PartnerName,
                    CustomerId = promocode.CustomerId,
                    IsUsed = promocode.IsUsed
                });
            }

            return Ok(response);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            // Ищем клиентов по предпочтениям и добавляем новый промокод в БД
            var preference = await _preferenceRepository.GetAsync(p => p.Name == request.Preference);
            if (preference == null)
                return NotFound("The specified preference was not found!");
            var customerPreferences = await _customerPreferenceRepository.GetAsync(cp => cp.PreferenceId == preference.First().Id);
            List<PromoCode> promocodesToAdd = new List<PromoCode>();
            foreach (var customerPreference in customerPreferences)
            {
                var promoCode = new PromoCode()
                {
                    Id = Guid.NewGuid(),
                    Code = request.PromoCode ?? "-",
                    ServiceInfo = request.ServiceInfo ?? "-",
                    BeginDate = request.BeginDate == null ? DateTime.Now : DateTime.ParseExact(request.BeginDate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                    EndDate = request.EndDate == null ? DateTime.Now.AddDays(PromocodeDelay) : DateTime.ParseExact(request.EndDate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                    PartnerName = request.PartnerName ?? "-",
                    PartnerManagerId = Guid.Parse(request.PartnerManagerId),
                    PreferenceId = customerPreference.PreferenceId,
                    CustomerId = customerPreference.CustomerId,
                    IsUsed = false
                };
                promocodesToAdd.Add(promoCode);
            }
            if (promocodesToAdd.Count > 0)
            {
                await _promocodeRepository.AddRangeAsync(promocodesToAdd);
                return Ok(request);
            }
            else
                return NotFound("No customers found with specified preference!");
        }
    }
}