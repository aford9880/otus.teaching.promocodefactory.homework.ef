﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;

        public PreferencesController(IRepository<Preference> preferenceRepository, IRepository<CustomerPreference> customerPreferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }

        /// <summary>
        /// Получение списка предпочтений с описанием
        /// </summary>
        /// <returns>Список список предпочтений</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetPreferencesAsync()
        {
            var response = new List<PreferenceResponse>();

            var preferences = await _preferenceRepository.GetAllAsync();
            foreach (var preference in preferences)
            {
                response.Add(new PreferenceResponse
                {
                    Id = preference.Id,
                    Name = preference.Name,
                    Description = preference.Description
                });
            }

            return Ok(response);
        }

        /// <summary>
        /// Добавление нового предпочтения
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreatePreferenceAsync(CreateOrEditPreferenceRequest request)
        {
            var preference = new Preference
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                Description = request.Descrition
            };

            await _preferenceRepository.AddAsync(preference);
            return Ok(preference);
        }

        /// <summary>
        /// Обновление предпочтения
        /// </summary>
        /// <param name="id">GUID</param>
        /// <param name="request">Запрос</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditPreferenceAsync(Guid id, CreateOrEditPreferenceRequest request)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            if (preference == null)
            {
                return NotFound();
            }

            preference.Name = request.Name ?? preference.Name;
            preference.Description = request.Descrition ?? preference.Description;

            await _preferenceRepository.UpdateAsync(preference);
            return Ok(preference);
        }

        /// <summary>
        /// Удаление предпочтения вместе с удалением из CustomerPreference
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var preference = _preferenceRepository.GetByIdAsync(id);
            if (preference != null)
            {
                await _preferenceRepository.DeleteAsync(id);
                var existingPreferences = await _customerPreferenceRepository.GetAsync(p => p.PreferenceId == id);
                if (existingPreferences != null && existingPreferences.Any())
                {
                    await _customerPreferenceRepository.DeleteRangeAsync(existingPreferences);
                }
            }
            else
                return NotFound();
            return Ok();
        }
    }
}