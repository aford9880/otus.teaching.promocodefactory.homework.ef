﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;

        public CustomersController(IRepository<Customer> customerRepository
            , IRepository<Preference> preferenceRepository
            , IRepository<CustomerPreference> customerPreferenceRepository
            , IRepository<PromoCode> promocodeRepository)
        {
            _customerRepository = customerRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns>Список клиентов</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = new List<CustomerShortResponse>();

            foreach (var customer in customers)
            {
                response.Add(new CustomerShortResponse
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email
                });
            }

            return Ok(response);
        }

        /// <summary>
        /// Получение клиента вместе с предпочтениями и выданными ему промокодами
        /// </summary>
        /// <param name="id">GUID клиента</param>
        /// <returns>Клиент вместе с предпочтениями и выданными ему промокодами</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();

            var response = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = new List<PreferenceResponse>(),
                PromoCodes = new List<PromoCodeShortResponse>()
            };

            var customerPreferences = await _customerPreferenceRepository.GetAsync(cp => cp.CustomerId == customer.Id);
            if (customerPreferences != null)
            {
                foreach (var customerPreference in customerPreferences)
                {
                    var preference = await _preferenceRepository.GetByIdAsync(customerPreference.PreferenceId);
                    response.Preferences.Add(new PreferenceResponse
                    {
                        Id = preference.Id,
                        Name = preference.Name,
                        Description = preference.Description
                    });
                }
            }

            var promocodes = await _promocodeRepository.GetAsync(cp => cp.CustomerId == customer.Id);
            if (promocodes != null)
            {
                foreach (var promoCode in promocodes)
                {
                    response.PromoCodes.Add(new PromoCodeShortResponse
                    {
                        Id = promoCode.Id,
                        Code = promoCode.Code,
                        ServiceInfo = promoCode.ServiceInfo,
                        BeginDate = promoCode.BeginDate.ToString(),
                        EndDate = promoCode.EndDate.ToString(),
                        PartnerName = promoCode.PartnerName,
                        IsUsed = promoCode.IsUsed
                    });
                }
            }

            return Ok(response);
        }

        /// <summary>
        /// Cоздание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(), 
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            await _customerRepository.AddAsync(customer);

            var preferences = request.PreferenceIds;
            foreach (var preferenceId in preferences)
            {
                var customerPreference = new CustomerPreference
                {
                    CustomerId = customer.Id,
                    PreferenceId = preferenceId
                };

                await _customerPreferenceRepository.AddAsync(customerPreference);
            }
            return Ok(customer);
        }

        /// <summary>
        /// Обновление данных клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id">GUID</param>
        /// <param name="request">Запрос</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            customer.FirstName = request.FirstName ?? customer.FirstName;
            customer.LastName = request.LastName ?? customer.LastName;
            customer.Email = request.Email ?? customer.Email;

            if (request.PreferenceIds != null && request.PreferenceIds.Any())
            {
                // Удаление существующих предпочтений клиента
                var existingPreferences = await _customerPreferenceRepository.GetAsync(p => p.CustomerId == id);
                if (existingPreferences != null && existingPreferences.Any())
                {
                    await _customerPreferenceRepository.DeleteRangeAsync(existingPreferences);
                }

                // Добавление новых предпочтений
                var preferences = request.PreferenceIds.Select(id => new CustomerPreference
                {
                    CustomerId = customer.Id,
                    PreferenceId = id
                });

                await _customerPreferenceRepository.AddRangeAsync(preferences);
            }

            await _customerRepository.UpdateAsync(customer);
            return Ok(customer);
        }

        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.DeleteCustomer(id);
            if (customer == null) return NotFound();
            return Ok();
        }
    }
}