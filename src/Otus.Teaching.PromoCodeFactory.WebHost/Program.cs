using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var dbContext = services.GetRequiredService<DataContext>();
                //dbContext.Database.EnsureDeleted(); // �������� ����
                //dbContext.Database.EnsureCreated(); // �������� ���� ��� ���������� ��������
                dbContext.Database.Migrate();
                if (!dbContext.Customers.Any())
                    SeedData(dbContext);
            }

            host.Run();
        }

        private static void SeedData(DataContext context)
        {
            // ���������� � ����
            context.Employees.AddRange(FakeDataFactory.Employees);
            foreach (Role role in FakeDataFactory.Roles)
                if (!context.Roles.Local.Any(r => r.Id == role.Id)) context.Roles.Add(role);            

            // ������� � ������������
            context.Customers.AddRange(FakeDataFactory.Customers);
            foreach(Preference preference in FakeDataFactory.Preferences)
                if (!context.Preferences.Local.Any(p => p.Id == preference.Id)) context.Preferences.Add(preference);
            
            context.SaveChanges();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.SetBasePath(hostingContext.HostingEnvironment.ContentRootPath);
                    config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                    config.AddEnvironmentVariables();
                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}